<?php

use WcSpinePayment\IpsConfig;
use WcSpinePayment\IpsRequest;
use WcSpinePayment\ThankYouPage;

class WcSpinePayment extends WC_Payment_Gateway
{
    public $version;
    private $testMode;
    private $config;
    /**
     * @var string|void
     */
    private $methodTitle;
    /**
     * @var string|void
     */
    private $methodDescription;
    /**
     * @var bool|WC_Order|WC_Order_Refund
     */
    private $order;
    private $brojOdobrenja;
    /**
     * @var mixed
     */
    private $txndatetime;
    /**
     * @var WC_Order|WP_Error
     */
    private $newOrder;

    /**
     * Class constructor, more about it in Step 3
     */
    public function __construct()
    {
        $this->id = 'wcSpinePayment'; // payment gateway plugin ID
        $this->version = '0.1';
        $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
        $this->methodTitle = __('IPS Skeniraj QR kod', 'spinePayment');
        $this->methodDescription = __('Svoju online porudžbinu možete platiti instant plaćanjem, metodom IPS skeniraj. Kada izaberete IPS skeniraj bićete preusmereni na stranicu na kojoj će Vam biti prikazan jednokratan IPS QR. Aplikacijom mobilnog bankarstva koju imate isntaliranu na svom mobilnom uređaju skeniraćete/preuzećete podatke iz generisanog IPS QR koda i plaćanje obavljate jednostavno, u sigurnom okruženju Vaše banke. Informacija o ishodu plaćanja biće Vam prikazana odmah po završetku obrade na našem sajtu ali će Vam biti dostavljena i putem imejla.',
            'spinePayment');
        $this->supports = [
            'products'
        ];

        // Method with all the options fields
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        $this->title = $this->get_option('title') ?: '';
        $this->description = $this->get_option('description') ?: '';
        $this->testMode = $this->get_option('testmode') ?: 'no';

        $config = [
            'clientId' => $this->get_option('clientIdTest'),
            'storeKey' => $this->get_option('storeKeyTest'),
            'endpoint' => $this->get_option('endpointTest'),
        ];
        if ($this->get_option('testmode') === "no") {
            $config = [
                'clientId' => $this->get_option('clientId'),
                'storeKey' => $this->get_option('storeKey'),
                'endpoint' => $this->get_option('endpoint'),
            ];
        }
        $this->config = $config;
        $this->addActionsAndFilters();
    }

    /**
     * Plugin options
     */
    public function init_form_fields()
    {
        $this->form_fields = [
            'enabled' => [
                'title' => 'Enable/Disable',
                'label' => 'Enable Spine Payment',
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ],
            'title' => [
                'title' => 'Title',
                'type' => 'text',
                'description' => 'This controls the title which the user sees during checkout.',
                'default' => $this->methodTitle,
                'desc_tip' => true,
            ],
            'description' => [
                'title' => 'Description',
                'type' => 'textarea',
                'description' => 'This controls the description which the user sees during checkout.',
                'default' => $this->methodDescription,
                'desc_tip' => true,
            ],
            'testmode' => [
                'title' => 'Test mode',
                'label' => 'Enable Test Mode',
                'type' => 'checkbox',
                'description' => 'Place the payment gateway in test mode using test API keys.',
                'default' => 'yes',
                'desc_tip' => true,
            ],
            'companyName' => [
                'title' => __('Company Name', 'spinePayment'),
                'type' => 'text',
                'description' => __('Company Name', 'spinePayment'),
            ],
            'companyAddress' => [
                'title' => __('Company Address', 'spinePayment'),
                'type' => 'text',
                'description' => __('Company Address', 'spinePayment'),
            ],
            'companyPib' => [
                'title' => __('PIB', 'spinePayment'),
                'type' => 'int',
                'description' => __('PIB', 'spinePayment'),
            ],
            'companyMb' => [
                'title' => __('MB', 'spinePayment'),
                'type' => 'int',
                'description' => __('MB', 'spinePayment'),
            ],
            'companyPhone' => [
                'title' => __('Telefon', 'spinePayment'),
                'type' => 'text',
                'description' => __('Telefon', 'spinePayment'),
            ],
            'companyEmail' => [
                'title' => __('Email', 'spinePayment'),
                'type' => 'email',
                'description' => __('Email', 'spinePayment'),
            ],
            'clientId' => [
                'title' => __('Merchant ID', 'spinePayment'),
                'type' => 'text',
                'description' => __('Merchant ID', 'spinePayment'),
            ],
            'storeKey' => [
                'title' => __('Store secret key', 'spinePayment'),
                'type' => 'text',
                'description' => __('Store secret key', 'spinePayment'),
            ],
            'endpoint' => [
                'title' => __('Service endpoint', 'spinePayment'),
                'type' => 'text',
                'description' => __('Service endpoint', 'spinePayment'),
            ],
            'clientIdTest' => [
                'title' => __('Test Merchant ID', 'spinePayment'),
                'type' => 'text',
                'description' => __('Test Merchant ID, used for test environment', 'spinePayment'),
            ],
            'storeKeyTest' => [
                'title' => __('Test Store secret key', 'spinePayment'),
                'type' => 'text',
                'description' => __('Test Store secret key, used for test environment', 'spinePayment'),
            ],
            'endpointTest' => [
                'title' => __('Test Service endpoint', 'spinePayment'),
                'type' => 'text',
                'description' => __('Test Service endpoint, used for test environment', 'spinePayment'),
            ],
        ];
    }


    /**
     * @param $message
     * @param array $data
     */
    public function log($message, array $data = []): void
    {
        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('Europe/Belgrade'));
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $data['userIp'] = $ip;

        $msg = "---$message--- > " . http_build_query($data, '', ', ');
        $entry = $dt->format('m-d-y H:i:s') . " > $msg, " . PHP_EOL;
        file_put_contents(ABSPATH . 'wp-content/uploads/' . 'spineLog.txt', $entry, FILE_APPEND);
    }

    /**
     * Generate the gateway button link.
     *
     * @return string
     */
    public function generateForm($order_id)
    {
        $order = wc_get_order($order_id);
        $config = new IpsConfig($order, $this->config);
        $request = new IpsRequest($config);

        return $request->generateForm();
    }

    /**
     * Function that checks the response code from the gateway service and handles the payment process based on those status codes
     * @throws WC_Data_Exception
     */
    public function processPayment()
    {
        $this->order = null;
//        if (count($_POST) === 0 && !isset($_GET['orderId'])) {
        if (count($_POST) === 0) {
			$this->log('Processing payment failed POST is empty');
            return false;
		}
		$orderId = null;
		if (isset($_POST['orderId'])) {
			$orderId = $_POST['orderId'];
		}
//		if (isset($_GET['orderId'])) {
//			$orderId = $_GET['orderId'];
//		}
		  
		if (!$orderId) {
			$this->log('Processing payment failed order id not provided.');
			return false;	
		}
		
//		if ($_GET['orderId'] !== '') {
//            return false;
//			$this->order = wc_get_order($orderId);
//			if ($this->order->get_status() === 'completed') {
//				return true;
//			}
//		}
			
                $this->log('Response found',
                    ['orderId' => $orderId, 'response' => http_build_query($_POST, '', ', '), 'txnDate' => $_POST['txndatetime'] ?? '']);

                $this->order = wc_get_order($orderId);
                if (strtolower($this->order->get_status()) === 'pending') {
                    $this->log('Starting processing payment');
                    if (!$this->checkHashResponse($_POST)) {
//                        die();
                    }
                    $this->brojOdobrenja = $_POST['response'];
                    $this->txndatetime = $_POST['txndatetime'];
                    $this->order->update_meta_data('brojOdobrenja', $this->brojOdobrenja);
                    $this->order->update_meta_data('txnDateTime', $this->txndatetime);
                    $this->order->save();
                    switch ($_POST['statusOdobrenja']) {
                        case '00':
                            $msg = '00';
                            $this->order->payment_complete();
                            $this->order->update_status('completed');
                            $this->order->save();
                            WC()->cart->empty_cart();
                            WC()->session->set('cart', []);
                            $this->log('Order payment complete', [
                                'orderId' => $this->order->get_id(),
                                'responseCode' => $msg
                            ]);
                            break;
						case '99':
                        case '05':
                            $msg = '05';
                            $this->cancelOrder($this->order);
                            $this->sendFailedTransactionEmail($this->order);
                            $this->log('Order payment failed', [
                                'orderId' => $this->order->get_id(),
                                'responseCode' => $msg
                            ]);
                            break;
                        case '82':
                            $msg = '82';
                            $this->cancelOrder($this->order);
                            $this->sendFailedTransactionEmail($this->order);
                            $this->log('Order payment failed', [
                                'orderId' => $this->order->get_id(),
                                'responseCode' => $msg
                            ]);
                            break;
                        default:
                            $msg = 'Unknown status code';
                            $this->cancelOrder($this->order);
                            $this->sendFailedTransactionEmail($this->order);
                            $this->log('Order payment failed', [
                                'orderId' => $this->order->get_id(),
                                'responseCode' => $msg
                            ]);
                            break;
                    }
                    return true;
                }
                $this->log('Order already processed');
                return false;
    }

    /**
     * @return WC_Order|WP_Error
     */
    public function getNewOrder()
    {
        return $this->newOrder;
    }

    /**
     * Return the payment url.
     *
     * @param int $orderId
     */
    public function process_payment($orderId)
    {
        $order = wc_get_order($orderId);
        if (get_current_user_id()) {
            update_user_meta(get_current_user_id(), 'gfSessionCartData',
                WC()->session->get('cart'));
        }
        WC()->cart->empty_cart();
        WC()->session->set('cart', []);
        return [
            'result' => 'success',
            'redirect' => $order->get_checkout_payment_url(true),
        ];
    }

    /**
     * Receipt page.
     *
     * Display text and a button to direct the user to ips page.
     */
    public function receipt_page($order)
    {
        echo '<p>Hvala vam na narudžbini, bićete preusmereni na stranicu za plaćanje</p>';
        echo $this->generateForm($order);
    }

    /**
     * The payment gateway demands that a new order is created when the transaction fails,
     * so we use this to recreate the order with a new order number
     *
     * @param WC_Order $order
     * @return WC_Order|WP_Error
     * @throws WC_Data_Exception
     */
    public function createNewOrderFromOrder(\WC_Order $order)
    {
        $paymentGateways = WC_Payment_Gateways::instance();
        $spinePayment = $paymentGateways->payment_gateways()['wcSpinePayment'];

        $billing = [
            'first_name' => $order->get_billing_first_name(),
            'last_name' => $order->get_billing_last_name(),
            'company' => $order->get_billing_company(),
            'email' => $order->get_billing_email(),
            'phone' => $order->get_billing_phone(),
            'address_1' => $order->get_billing_address_1(),
            'address_2' => $order->get_billing_address_2(),
            'city' => $order->get_billing_city(),
            'state' => $order->get_billing_state(),
            'postcode' => $order->get_billing_postcode(),
            'country' => $order->get_billing_country()
        ];
        $shipping = [
            'first_name' => $order->get_shipping_first_name(),
            'last_name' => $order->get_shipping_last_name(),
            'company' => $order->get_shipping_company(),
            'address_1' => $order->get_shipping_address_1(),
            'address_2' => $order->get_shipping_address_2(),
            'city' => $order->get_shipping_city(),
            'state' => $order->get_shipping_state(),
            'postcode' => $order->get_shipping_postcode(),
            'country' => $order->get_shipping_country()
        ];
        $newOrder = wc_create_order([
            'status' => 'pending',
            'customer_id' => get_current_user_id()
        ]);

        $newOrder->set_payment_method($spinePayment);
        foreach ($order->get_items() as $item) {
            $newOrder->add_item($item);
        }
        foreach( $order->get_items( 'shipping' ) as $shippingItemId => $shippingItem ){
            $item = new WC_Order_Item_Shipping();
            $item->set_method_title($shippingItem->get_method_title());
            $item->set_method_id($shippingItem->get_method_id());
            $item->set_total($shippingItem->get_total());
            $item->calculate_taxes(['country' => $order->get_shipping_country()]);
            $newOrder->add_item($item);
        }
        $newOrder->set_address($billing, 'billing');
        $newOrder->set_address($shipping, 'shipping');
        $newOrder->calculate_totals();
        $newOrder->save();
        return $newOrder;
    }

    /**
     * Hooks stuff into wp
     */
    private function addActionsAndFilters()
    {
        // This action hook saves the settings
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_receipt_' . $this->id, [$this, 'receipt_page']);
        add_action('woocommerce_email_order_details', [$this, 'addRequiredInfoToEmails'], 9);
    }

    public function addRequiredInfoToEmails($order)
    {
        if ($order->get_payment_method() === 'wcSpinePayment') {
            $this->getMerchantInfo();
            $this->getTransactionInfo();
        }
    }

    public function paymentRetry()
    {
        $order = wc_get_order($_POST['orderId']);
        $url = get_home_url() . '/cart/';
        if ($order) {
            $this->cancelOrder($order);
            $newOrder = $this->createNewOrderFromOrder($order);
            $url = $newOrder->get_checkout_payment_url();
        }

        wp_send_json_success(['checkoutUrl' => $url]);
    }

    /**
     * Cancels order
     * @param WC_Order $order
     */
    private function cancelOrder(WC_Order $order)
    {
        $order->update_status('failed', 'spine', true);
        $order->save();
        WC()->cart->empty_cart();
    }

    /**
     * Sends the failed transaction email
     * Template is located in templates/emails
     * @param $order
     */
    private function sendFailedTransactionEmail($order)
    {
        $email = $order->get_billing_email();
        $emailHeading = 'Transakcija neuspešna';
        ob_start();
        include(PLUGIN_DIR . '/templates/emails/customerFailedOrder.php');
        $emailContent = ob_get_clean();
        $subject = 'Vaša narudžbina je otkazana';
        wc()->mailer()->send($email, $subject, $emailContent);
    }

    /**
     *  Gets the merchant info from the settings and includes the template for displaying it.
     */
    public function getMerchantInfo()
    {
        $companyName = $this->get_option('companyName');
        $companyAddress = $this->get_option('companyAddress');
        $companyPib = $this->get_option('companyPib');
        $companyMb = $this->get_option('companyMb');
        $companyPhone = $this->get_option('companyPhone');
        $companyEmail = $this->get_option('companyEmail');
        include PLUGIN_DIR . '/templates/merchantInfo.php';
    }

    /**
     * Display the required transaction info for the gateway
     */
    public function getTransactionInfo($order = null)
    {
        $txnDateTime = $this->getTxndatetime();
        $brojOdobrenja = $this->getBrojOdobrenja();
        if ($order) {
            $txnDateTime = $this->order->get_meta('txnDateTime', true);
            $brojOdobrenja = $this->order->get_meta('brojOdobrenja', true);
        }
        if ($txnDateTime !== '' && $txnDateTime !== null) {
            $dt = new DateTime();
            $dt->setTimestamp(strtotime($txnDateTime));
            $txnDateTime = $dt->format('m/d/Y H:i:s');
        }
        include PLUGIN_DIR . '/templates/transactionInfo.php';
    }

    public function getBrojOdobrenja()
    {
        return $this->brojOdobrenja;
    }

    public function getTxndatetime()
    {
        return $this->txndatetime;
    }

    public function get_order()
    {
        return $this->order;
    }

    public function getThankYouPageUrl()
    {
        $page = new ThankYouPage('ips-thankyou-page');

        return $page->getPageUrl() . '?orderId=' . wc_get_order_id_by_order_key($_GET['key']);
    }

    private function checkHashResponse($post): bool
    {
        if(!isset($post['orderId'])) {
            return false;
        }
        $this->log('Checking hash validity');
        $order = wc_get_order($post['orderId']);
        $clientId = $this->config['clientId'];
        $dt = new DateTime();
        $dt->setTimestamp(strtotime($post['txndatetime']));
        $txnDateTime = $dt->format('Y:m:d-h:i:s');
        $amount = number_format($order->get_total(), 2, ',', '');
        $currencyCode = 941;
        $storeKey = $this->config['storeKey'];
        $responseCode = $_POST['statusOdobrenja'];
        $pattern = $clientId . $txnDateTime . str_replace(',', '.', $amount) . $currencyCode . $responseCode. $storeKey;
        $correctHash = hash('sha256', $pattern);
        if (!isset($_POST['hashRe'])) {
            $this->log('Hash param not received');
            return false;
        }
        if ($_POST['hashRe'] !== $correctHash) {
            $this->log('Invalid hash', ['receivedHash' => $_POST['hashRe'], 'correctHash' => $correctHash]);
            return false;
        }
        $this->log('Hash is valid');
        return true;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }
}