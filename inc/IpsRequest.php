<?php
namespace WcSpinePayment;
/**
 * Class RequestParams.
 * Class represents parameters required in order to communicate with payment gateway.
 *
 */
class IpsRequest
{
    /**
     * @var IpsConfig
     */
    private $config;

    /**
     * Constructor method.
     * Must receive config object with required parameters.
     *
     * @param $config
     *
     */
    public function __construct(IpsConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Generate form which needs to be POSTed to gateway.
     *
     * @return string
     */
    public function generateForm()
    {
        return '<form action="' . esc_url($this->config->getEndPoint()) . '" method="post" id="ips_payment_form">' .
            $this->config->getMandatoryParams() .
            $this->generateButtonsAndJs() .
            '</form>';
    }

    private function generateButtonsAndJs()
    {
        $buttonTxt = 'Platite putem IPS QR koda';
        $userMessageTxt = 'Hvala za narudžbu. Sada ćete biti preusmereni na stranicu za plaćanje.';
        $cancelOrderTxt = 'Otkažite narudžbinu';

        return '<input type="submit" class="button-alt" id="submit_ips_payment_form" value="' . $buttonTxt . '" /> 
				<a class="button cancel" href="' . $this->config->getCancelUrl() . '">' . $cancelOrderTxt . '</a>
				<script type="text/javascript">
					jQuery(function(){
						jQuery("body").block(
							{
								message: "' . $userMessageTxt . '",
								overlayCSS:
								{
									background: "#fff",
									opacity: 0.6
								},
								css: {
									padding:        20,
									textAlign:      "center",
									color:          "#555",
									border:         "3px solid #aaa",
									backgroundColor:"#fff",
									cursor:         "wait"
								}
							});
						jQuery( "#submit_ips_payment_form" ).click();
					});
				</script>';
    }
}
