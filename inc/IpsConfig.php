<?php
namespace WcSpinePayment;
class IpsConfig
{

    private $clientId;
    private $storeKey;
    private $endpoint;
    private $amount;
    private $successUrl;
    private $cancelUrl;
    private $oid;
    private $order;

    private $mandatoryParams;
    /**
     * @var mixed
     */
    private $spinePayment;
    /**
     * @var string
     */
    private $thankYouPageUrl;

    public function __construct(\WC_Order $order, $config)
    {
        $this->storeKey = $config['storeKey'];
        $this->clientId = $config['clientId'];
        $this->endpoint = $config['endpoint'];
        $this->order = $order;
        $paymentGateways = \WC_Payment_Gateways::instance();
        /** @var \WcSpinePayment $spinePayment */
        $spinePayment = $paymentGateways->payment_gateways()['wcSpinePayment'];
        $this->amount = number_format($order->get_total(), 2, ',', '');
        $thankYouPageUrl = $spinePayment->getThankYouPageUrl();
        $this->successUrl = $thankYouPageUrl;
        $this->cancelUrl = $thankYouPageUrl;
        $this->oid = $order->get_id();
        $this->processData();
    }

    /**
     * Creates form params for communication with Nestpay gateway.
     */
    public function getMandatoryParams()
    {
        $form = '' . PHP_EOL;

        foreach ($this->mandatoryParams as $key => $param) {
            $form .= '<input type="hidden" name="' . $key . '" value="' . $param . '"/>' . PHP_EOL;
        }
        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('Europe/Belgrade'));
        $entry = $dt->format('m-d-y H:m:s').' REQUEST'. ', '. serialize($this->mandatoryParams) . PHP_EOL;
        file_put_contents(ABSPATH . 'wp-content/uploads/' . 'spineResponseLog.txt', $entry , FILE_APPEND);
        return $form;
    }

    /**
     * Compiles hash parameter required for sending along with other parameters.
     *
     * @return string
     */
    private function compileHash()
    {
        $pattern = $this->clientId . $this->order->get_date_created()->format('Y:m:d-h:i:s') . str_replace(',', '.', $this->amount) . 941 . $this->storeKey;
        return hash('sha256', $pattern);
    }

    /**
     * Entry point for preparing data to be sent together with user to gateway.
     *
     * @return void
     */
    protected function processData()
    {
        $this->mandatoryParams['storeId'] = $this->clientId;
        $this->mandatoryParams['txntype'] = '001';
        $this->mandatoryParams['timezone'] = 'Europe/Belgrade'; // RFC3339
//        var_dump($this->order->get_date_created()->format('Y-m-d\TH:i:s.v') . 'Z');
//        die();
        $this->mandatoryParams['txndatetime'] = $this->order->get_date_created()->format('Y:m:d h:i:s');
//        $this->mandatoryParams['txndatetime'] = $this->order->get_date_created()->format('Y-m-d\TH:i:s.v') . 'Z';
        $this->mandatoryParams['returnOkUrl'] = $this->successUrl;
        $this->mandatoryParams['returnFailUrl'] = $this->cancelUrl . '&cancel=true';
        $this->mandatoryParams['hash'] = $this->compileHash();
        $this->mandatoryParams['orderId'] = $this->oid;
        $this->mandatoryParams['iznos'] = str_replace(',', '.', $this->amount);
        $this->mandatoryParams['valuta'] = 941;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }

    public function getStoreKey()
    {
        return $this->storeKey;
    }

    public function getClientId()
    {
        return $this->clientId;
    }
}