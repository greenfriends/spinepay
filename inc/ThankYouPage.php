<?php


namespace WcSpinePayment;


class ThankYouPage
{
    public $pageSlug;

    /**
     * ThankYouPage constructor.
     * @param string $pageSlug
     */
    public function __construct($pageSlug)
    {
        $this->pageSlug = $pageSlug;
    }
    /**
     * Adds the shortcode for the thank you page inside wordpress registered shortcodes
     */
    public function setupShortcode()
    {
        add_shortcode('ips_thank_you_page', [$this, 'thankYouPageView']);
        add_action('wp_enqueue_scripts', function (){
            wp_enqueue_script('refreshCartCount', PLUGIN_DIR_URI.'/assets/js/refreshCartCount.js', ['wc-cart-fragments'],'0.0.1', true);
        });
    }

    /**
     * Creates the custom thank you page needed for our gateway to process payments.
     */
    private function createPage()
    {
        $isPageCreated = get_page_by_title('Ips thankyou page', 'OBJECT', 'page');
        // Check if the page already exists
        if (empty($isPageCreated)) {
            $pageId = wp_insert_post(
                [
                    'comment_status' => 'close',
                    'ping_status' => 'close',
                    'post_author' => 1,
                    'post_title' => 'Ips thankyou page',
                    'post_name' => $this->pageSlug,
                    'post_status' => 'publish',
                    'post_content' => '[ips_thank_you_page]',
                    'post_type' => 'page'
                ]
            );
            add_filter('display_post_states', static function ($post_states, $post) use ($pageId) {
                if ($post->ID == $pageId) {
                    $post_states[] = 'Do not delete this page';
                }
                return $post_states;
            }, 10, 2);
        }
    }

    /**
     * Callback for the [ips_thank_you_page] shortcode.
     * @return false|string
     */
    public function thankYouPageView()
    {
        ob_start();
        include PLUGIN_DIR . '/templates/thankYouPage.php';
        return ob_get_clean();
    }

    /**
     * Calls function used to create thank you page.
     * hooked into plugin activation
     */
    public function activate()
    {
        $this->createPage();
    }

    /**
     * Returns the thank you page url
     * @return string
     */
    public function getPageUrl()
    {
        return get_home_url() . '/' . $this->pageSlug . '/';
    }
}