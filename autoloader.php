<?php
$incDir = new DirectoryIterator(__DIR__ . '/inc');
foreach ($incDir as $item) {
    //Wc spine payment needs to be loaded after all plugins are loaded, so skip it here
    if ($item->getFilename() !== 'WcSpinePayment.php' && is_file($item->getPathname())){
        require_once ($item->getPathname());
    }
}