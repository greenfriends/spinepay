<div class="merchantDetailsWrapper">
    <h2><?= __('Podaci o trgovcu:', 'spinePayment') ?></h2>
    <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
        <li class="woocommerce-order-overview__order order">
            <?= __('Naziv:', 'spinePayment') ?>
            <strong><?= $companyName ?></strong>
        </li>
        <li class="woocommerce-order-overview__order order">
            <?= __('Adresa:', 'spinePayment') ?>
            <strong><?= $companyAddress ?></strong>
        </li>
        <li class="woocommerce-order-overview__order order">
            <?= __('Pib:', 'spinePayment') ?>
            <strong><?= $companyPib ?></strong>
        </li>
        <li class="woocommerce-order-overview__order order">
            <?= __('Maticni broj:', 'spinePayment') ?>
            <strong><?= $companyMb ?></strong>
        </li>
        <li class="woocommerce-order-overview__order order">
            <?=__('Telefon:','spinePayment')?>
            <strong><?=$companyPhone?></strong>
        </li>
        <li class="woocommerce-order-overview__order order">
            <?=__('Email:','spinePayment')?>
            <strong><?=$companyEmail?></strong>
        </li>
    </ul>
</div>
