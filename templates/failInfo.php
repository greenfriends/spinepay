<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php
    esc_html_e('Nažalost, narudžbina ne može biti izvršena. Molimo Vas, probajte opet',
        'woocommerce'); ?></p>
<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
    <button onclick="paymentRetry()" class="button pay">
        Pokušajte ponovo
    </button>
    <?php if (is_user_logged_in()) : ?>
        <a href="<?= esc_url(wc_get_page_permalink('myaccount'))?>" class="button pay">Moj nalog</a>
    <?php endif; ?>
</p>
