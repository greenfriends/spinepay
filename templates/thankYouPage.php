<?php

defined('ABSPATH') || exit;

$orderId = 'Order not found';
$order = null;
$orderStatus = 'N/A';
$spinePayment = new WcSpinePayment();
$logMessage = 'USER OPENED THANK YOU PAGE';
$context = ['orderId'];


//save timestamp to redis
$waitForPost = false;
if($_SERVER['REQUEST_METHOD'] !== 'POST' && isset($_GET['orderId'])) {
    $orderId = (int) $_GET['orderId'];
    $spinePayment->log($logMessage, ['orderId' => $orderId, 'Request method' => $_SERVER['REQUEST_METHOD']]);
    $order = wc_get_order($orderId);
    $expiry = $order->get_meta('ipsReloadExpiry',true);
    if($expiry === '') {
        $order->update_meta_data('ipsReloadExpiry', time() + 180);
        $order->save();
    }
    //wait for POST
    // @TODO if timestamp expired cancel order
    if ($order->get_status() !== 'completed') {
        $spinePayment->log('WAITING TO PROCESS', ['orderId' => $orderId, 'Request method' => $_SERVER['REQUEST_METHOD']]);
        $waitForPost = true;
    } else {
        $spinePayment->setOrder($order);
    }
    if($expiry && time() >= $expiry) {
        $spinePayment->log('PROCESSING TIMEOUT', ['orderId' => $orderId, 'Request method' => $_SERVER['REQUEST_METHOD']]);
        $waitForPost = false;
    }
} else {
    $orderId = $_POST['orderId'];
    $spinePayment->log('PROCESSING', ['orderId' => $orderId, 'Request method' => $_SERVER['REQUEST_METHOD']]);
    $order = wc_get_order($orderId);
    $spinePayment->processPayment();
    $order = $spinePayment->getOrder();
}

    if ($order) {
        $orderId = $order->get_id();
        $orderStatus = $order->get_status();
    }
//} else {
//    $spinePayment->log($logMessage, ['orderId' => $orderId,'Request method' => $_SERVER['REQUEST_METHOD']]);
//}

?>
<div class="woocommerce-order">
    <?php
    if ($waitForPost) {
    ?>
    <p>Obrađujemo transakciju, molimo sačekajte.</p>
    <?php
    } else {
        if ($order) {
            do_action('woocommerce_before_thankyou', $order->get_id());
            if ($order->has_status('failed') ||
                $order->has_status('cancelled') ||
                $order->has_status('refunded') ||
                $order->has_status('on-hold') ||
                $order->has_status('pending')){
                include __DIR__ . '/failInfo.php';
            } else {
                include __DIR__ . '/successInfo.php';
            }
            do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id());
            do_action('woocommerce_thankyou', $order->get_id());
        } else {
            if (is_user_logged_in()) {
                $savedCart = get_user_meta(get_current_user_id(), 'gfSessionCartData', true);
                if ($savedCart && empty(WC()->session->cart)) {
                    WC()->session->set('cart', $savedCart);
                    update_user_meta(get_current_user_id(), 'gfSessionCartData', '');
                }
            }
            include __DIR__ . '/failInfo.php';
        }
    }
    ?>
</div>
<script>
<?php if ($waitForPost) { ?>
    setTimeout(function() {
        window.location.reload(1);
    }, 10000);
<?php } ?>

    let clicked = false;

    function paymentRetry() {
        if (clicked === false) {
            clicked = true;
            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: '<?=admin_url('admin-ajax.php')?>',
                data: {action: "orderRetry", orderId: '<?=$orderId?>'},
                success: function (response) {
                    if (response) {
                        location.href = response.data['checkoutUrl']
                    }
                }
            })
        }
    }
</script>
