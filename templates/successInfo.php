<h2><?=__('Podaci o transakciji:', 'spinePayment')?></h2>
<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
    <?= apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Hvala, Vaša narudžbina je uspešno plaćena.',
                                                                             'woocommerce'), $order) ?>
</p>
<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
    <li class="woocommerce-order-overview__order order">
        <?php esc_html_e('Order number:', 'woocommerce'); ?>
        <strong><?= $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
    </li>
    <?php if (is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email()) : ?>
        <li class="woocommerce-order-overview__email email">
            <?php esc_html_e('Email:', 'woocommerce'); ?><strong><?= $order->get_billing_email()?></strong>
        </li>
    <?php endif; ?>
    <li class="woocommerce-order-overview__total total">
        <?php esc_html_e('Total:', 'woocommerce'); ?> <strong><?= $order->get_formatted_order_total()?></strong>
    </li>
    <?php
    $spinePayment->getTransactionInfo($order) ?>
</ul>
<?php $spinePayment->getMerchantInfo() ?><?php
