<?php
/**
 * Plugin Name: WooCommerce Spine
 * Description: Receive payments using the QR code
 * Author: Green Friends
 * Version: 0.0.3
 */

/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */

use WcSpinePayment\ThankYouPage;

define('PLUGIN_DIR', __DIR__);
define('PLUGIN_DIR_URI', plugin_dir_url(__FILE__));
add_filter('woocommerce_payment_gateways', 'gfAddSpinePayment');
require(__DIR__ . '/autoloader.php');
/**
 * Adds our gateway to woocommerce gateways.
 *
 * @param $gateways
 * @return mixed
 */
function gfAddSpinePayment($gateways)
{
    $gateways[] = 'WcSpinePayment'; // your class name is here
    return $gateways;
}

add_action('plugins_loaded', 'gfInitSpinePaymentClass');

/**
 * Requires class file for our gateway
 */
function gfInitSpinePaymentClass()
{
    require_once(__DIR__ . '/inc/WcSpinePayment.php');
    load_plugin_textdomain('spinePayment');
}

/**
 * Adds a direct link to the settings page for gateway on plugins screen.
 *
 * @param $links
 * @return array
 */
function settingsLink($links)
{
    $settings_url = add_query_arg(
        [
            'page' => 'wc-settings',
            'tab' => 'checkout',
            'section' => 'wcspinepayment',
        ],
        admin_url('admin.php')
    );

    $plugin_links = [
        '<a href="' . esc_url($settings_url) . '">' . __('Settings', 'spinePayment') . '</a>',

    ];

    return array_merge($plugin_links, $links);
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'settingsLink');
$thankYouPage = new ThankYouPage('ips-thankyou-page');
$thankYouPage->setupShortcode();
register_activation_hook(__FILE__, [$thankYouPage, 'activate']);

//maybe we will need this do not delete
//function customThankYouRedirect()
//{
//    $thankYouPage = new ThankYouPage();
//    /* do nothing if we are not on the appropriate page */
//    if( !is_wc_endpoint_url( 'order-received' ) || empty( $_GET['key'] ) ) {
//        return;
//    }
//    $orderId = wc_get_order_id_by_order_key( $_GET['key'] );
//    $order = wc_get_order( $orderId );
//
//    if( 'WcSpinePayment' === $order->get_payment_method() ) {
//        wp_redirect( $thankYouPage->getPageUrl().'?orderId=' . $orderId);
//        exit;
//    }
//}
//add_action( 'woocommerce_thankyou', 'customThankYouRedirect');

//adds ajax hook needed for recreating order on payment retry click
add_action("wp_ajax_orderRetry", 'orderRetry');
add_action("wp_ajax_nopriv_orderRetry", 'orderRetry');
function orderRetry()
{
    if (isset($_POST['orderId'])){
        $paymentGateways = WC_Payment_Gateways::instance();
        /** @var WcSpinePayment $spinePayment */
        $spinePayment = $paymentGateways->payment_gateways()['wcSpinePayment'];
        $spinePayment->paymentRetry();
    } else {
        WC_Cart_Session::get_cart_from_session();
    }
}