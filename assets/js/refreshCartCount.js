jQuery(function ($) {
    $(window).on('pageshow', function (e) {
        $('.widget_shopping_cart_content').empty();
        $(document.body).trigger('wc_fragment_refresh');
    });
})